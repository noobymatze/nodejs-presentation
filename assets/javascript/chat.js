var chat = function(config) {
	"use strict";

	var text = function(string) { return document.createTextNode(string); };

	var element = function(name) { return document.createElement(name); }

	var append = function(element, list) {
		for( var item in list ) {
			console.log(list[item] + ": " + element);
			element.appendChild(list[item]);
		}
	};

	var appendMessage = function(message) {
		var p = element('P');
		p.appendChild(text(message));
		messages.appendChild(p);
	};

	var prependMessage = function(message) {
		var p = element('P');
		p.appendChild(text(message));
		messages.insertBefore(p, messages.firstChild);
	};

	var bindSocketEvents = function(socket, handlers) {
		for( var event in handlers ) {
			socket.on(event, handlers[event]);
		}
	};

	var html = function(element, string) { element.innerHTML = string; };

	var thiz = this,
		config = config || {},
		connect  = element('BUTTON'),
		messages = element('SECTION'),
		message  = element('INPUT'),
		sender	 = element('BUTTON'),
		status 	 = element('P'),
		url 	 = 'http://localhost:8080/chat',
		socket 	 = null;

	var connected = function() {
		html(status, 'Verbunden mit ' + url);
		status.setAttribute('class', 'status ok');
		connect.setAttribute('class', 'connection ok');
	};

	var disconnected = function() {
		html(status, 'Verbindung abgebrochen...');
		status.setAttribute('class', 'status error');
	};

	var onbroadcast = function(json) { prependMessage(json.message); };

	var send = function(object) {
		socket.emit('message', object);
	};

	var sendMessage = function() {
		send({ 'message': message.value });
		prependMessage(message.value);
		message.value = "";
	};

	var initSocket = function() {
		socket = io.connect('http://192.168.0.11/chat');
		bindSocketEvents(socket, {
			'connect'	: connected,
			'disconnect': disconnected,
			'broadcast' : onbroadcast
		});
	};

	var initDOM = function(parent) {
		var fragment = document.createDocumentFragment();

		status.setAttribute('class', 'status error');
		messages.setAttribute('class', 'messages');
		message.setAttribute('class', 'message');
		connect.setAttribute('class', 'connection');
		sender.appendChild(text('Sende Nachricht'));
		messages.appendChild(connect);

		status.appendChild(text('Nicht Verbunden...'));
		append(fragment, [status, messages, message, sender]);

		message.addEventListener('keyup', function(e) {
			if( e.keyCode === 13 ) sendMessage();
		}, false);

		sender.addEventListener('click', sendMessage, false);
		connect.addEventListener('click', function() {
			if( socket === null ) {
				initSocket();
				connected();
			}
			else {
				socket.disconnect();
				socket = null;
				connect.setAttribute('class', 'connection');
			}
		}, false);

		parent.appendChild(fragment);
	};

	var init = function() {
		if( config.parentID === undefined ) {
			alert('No parent element for Chat selected.');
			return;
		}

		initDOM(document.getElementById(config.parentID));
		return this;
	};

	return {
		init: init,
		send: send
	};
};