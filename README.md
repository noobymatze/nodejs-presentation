# Presentation

You need to download Nodejs from [http://www.nodejs.org/download/](http://www.nodejs.org/download/), set the PATH variables for node and you're good to go.

After installing unzip the presentation, fire up the terminal and change directory to the unzipped folder. There, execute the following two commands:
	
	$ npm install
	$ node app.js

The first one will install all needed packages, defined in package.json. The second one will start the presentation on port 8080. 

Have fun and if you want to know what's going on, read the source, Luke !