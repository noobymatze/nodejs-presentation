var app 		= module.exports = require('express')(),
	server 		= require('http').createServer(app),
	application = require('./controllers/application.js');
	chat 		= require('./controllers/chat.js'),
	io 			= require('socket.io').listen(server),
	webSocket	= null,
	PORT 		= 8080,

// Configuring directories.
app.set('views', __dirname + '')

app.get('/', application.index);
app.get('/chat', chat.index);
app.get('/assets/*', application.assets);

server.listen(PORT, function() {
	console.log('Server running on port: ' + PORT);
});

webSocket = io.of('/chat').on('connection', chat.connected);