var	views = require('../app.js').get('views'),
	Chat = {

	recieved: function(json) {
		console.log(json);
	},

	connected: function(socket) {
		socket.on('message', function(json) {
			socket.broadcast.emit('broadcast', json);
			console.log(json);
		});
		
		socket.on('disconnect', function() {
			console.log('User disconnected');
		});
	},

	disconnected: function() {
		console.log('User disconnected.');
	},

	index: function(request, response) {
		response.render(views + '/chat/chat.jade');
	},
};

module.exports.connected = Chat.connected;
module.exports.index = Chat.index;