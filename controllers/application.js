var url = require('url');
var views = require('../app.js').get('views');

module.exports.index = function(request, response) {
	response.render(views + '/index.jade');
}

module.exports.assets = function(request, response) {
	var file = url.parse(request.url).pathname;

	console.log("Requested resource: " + file);

	if( file.substring(0, 1) === '/' ) {
		file = file.substring(1);
	}

	response.sendfile(file);
}